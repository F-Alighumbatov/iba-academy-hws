package HW6i7;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        char symbol = sc.next().charAt(0);
        if (Character.isUpperCase(symbol)){
            System.out.println("Yes");
        }
        else System.out.println("No");

    }
}
